import { Component } from '@angular/core';
import { MapSearchComponent } from './search/map-search/map-search.component';
import { AuthenticationService } from './shared/services/authentication.service';
import { ILoggedIn } from './shared/interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private loggedIn : ILoggedIn;
  
  constructor(private authenticationService : AuthenticationService) {
    this.loggedIn = authenticationService.loggedIn;
  }

  private logout() : void {
    this.authenticationService.logout();
  }


}
