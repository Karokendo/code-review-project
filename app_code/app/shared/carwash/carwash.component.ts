import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ICarwash } from './../interfaces';
import { CarwashService } from './../services/carwash.service';

@Component({
    selector: 'carwash',
    templateUrl: 'carwash.component.html',
    styleUrls: ['carwash.component.css']
})
export class CarwashComponent implements OnInit {

    private carwash: ICarwash = null;
    private errorMessage: string;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private carwashService: CarwashService,
        private chRef: ChangeDetectorRef) {
    }

    ngOnInit() {
        console.log("CarwashComponent - OnInit");
        this.route.params.subscribe(
            params => {
                let id = +params['id'];
                this.getCarwash(id);
            });
    }

    getCarwash(id: number) {
        this.carwashService.getCarwash(id).subscribe(
            (data: ICarwash )=> {
                this.carwash = data;
                console.log("Data: " + data.id);
                console.log("Car: " + this.carwash.id);
                this.chRef.detectChanges();
            },
            error => this.errorMessage = <any>error);
    }

    goBack() {
        this.location.back();
    }

}