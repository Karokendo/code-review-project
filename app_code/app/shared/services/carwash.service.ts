import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { ICarwash, IArea } from '../interfaces';

@Injectable()
export class CarwashService {
    
    private url: string = 'rest/';
    
    constructor(private http: Http) { }
    
    getCarwashesSummary() : Observable<ICarwash[]> {
        return this.http.get(this.url + 'carwashes/')
                   .map((resp: Response) => resp.json())
                   .catch(this.handleError);
    }

    getCarwashesByArea(area: IArea):  Observable<ICarwash[]> {
        return this.http.post(this.url + 'carwashes/area', area)
                   .map((resp: Response) => resp.json())
                   .catch(this.handleError);
    }
    
    getCarwash(id: number): Observable<ICarwash> {
        return this.http.get(this.url + 'carwashes/' + id)
                   .map((resp: Response) => resp.json())
                   .catch(this.handleError);
    }

    updateCarwash(carwash: ICarwash) {       
      return this.http.put(this.url + 'carwashes/' + carwash.id, carwash)
                 .map((response: Response) => response.json())
                 .catch(this.handleError);
    }


    
    handleError(error: any) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
    
}
