import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class MapService {
    public location = new EventEmitter<google.maps.LatLng>();

    updateLocation(latLng: google.maps.LatLng) {
        this.location.emit(latLng);
    }
}

