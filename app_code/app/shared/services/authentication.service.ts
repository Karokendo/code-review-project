import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { JwtHelper } from 'angular2-jwt';
import { tokenNotExpired } from 'angular2-jwt';
import { ILoggedIn } from './../interfaces';

@Injectable()
export class AuthenticationService {

    tokenId: string = "id_token";
    private jwtHelper: JwtHelper = new JwtHelper();
    loggedIn: ILoggedIn = { in: false };


    constructor(private http: Http) { }

    login(email: string, password: string) {
        return this.http.post('/rest/authenticate/', { email: email, password: password })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let httpResp = response.json();
                if (httpResp && httpResp.token && this.jwtHelper.isTokenExpired(httpResp.token, 1000)) {
                    localStorage.setItem(this.tokenId, JSON.stringify(httpResp.token));
                }
            });
    }

    logout(): void {
        // remove user from local storage to log user out
        localStorage.removeItem(this.tokenId);
        this.loggedIn.in = false;
    }

    notExpired(): boolean {
        return tokenNotExpired();
    }
}