export interface ICarwash {
    id: number;
	version: number;
	name: string;
	address: IAddress;
	owner: IOwner;
	country: ICountry;
	features: string[];
	openningHours: IOpenningHours[];
}

export interface ICountry {
	id: number;
	version: number;
	name: string;
	code: string;
	carwashes: string;
}

export interface IOwner {
	id: number;
	version: number;
	name: string;
	username: string;
	password: string;
	phone: string;
	email: string;
	carwashes: string;
}

export interface IAddress {
	id: number;
	version: number;
	lat: number;
	lng: number;
	lineone: string;
	linetwo: string;
	city: string;
	carwash: string;
}

export interface IArea {
	ne: IPoint;
	sw: IPoint;
}

export interface IPoint {
	lat: number;
	lng: number;
}

export interface IOpenningHours {
	day: string;
	from: string;
	to: string;
	closed: boolean;
	allDay: boolean;
}

export interface ILoggedIn {
	in: boolean;
}