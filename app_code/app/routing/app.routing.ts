import { Routes, RouterModule } from '@angular/router';
import { CarwashComponent } from './../shared/carwash/carwash.component';
import { SearchComponent } from './../search/search.component';

const APP_ROUTES: Routes = [
    {
        path: 'carwash/:id',
        component: CarwashComponent
    }, {
        path: '',
        component: SearchComponent
    }, {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    },
]

export const AppRouting = RouterModule.forRoot(APP_ROUTES);
