import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GoogleplaceDirective } from 'angular2-google-map-auto-complete/directives/googleplace.directive';
import { AuthModule } from './shared/modules/auth.module';

import { AppComponent } from './app.component';
import { MapSearchComponent } from './search/map-search/map-search.component';
import { SearchComponent } from './search/search.component';
import { NavComponent } from './nav/nav.component';
import { MapComponent } from './search/map/map.component';
import { CarwashComponent } from './shared/carwash/carwash.component';
import { CarwashService } from './shared/services/carwash.service';
import { MapService } from './shared/services/map.service';
import { AppRouting } from './routing/app.routing';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './shared/services/authentication.service';

@NgModule({
  declarations: [
    AppComponent,
    MapSearchComponent,
    NavComponent,
    GoogleplaceDirective,
    MapComponent,
    CarwashComponent,
    SearchComponent,
    LoginComponent
  ],
  imports: [
    AuthModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    CommonModule,
    AppRouting
  ],
  providers: [
    CarwashService,
    MapService,
    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
