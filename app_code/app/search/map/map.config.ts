import { styles } from './map.styles';

export var options: any = {
  center: { lat: 51.1079, lng: 17.0385 },
  zoom: 13,
  styles: styles,
  fullscreenControl: false,
  mapTypeControl: false,
  streetViewControl: false,
  //maxZoom: 20,
  minZoom: 3
} 