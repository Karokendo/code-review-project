import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

import { CarwashService } from './../../shared/services/carwash.service';
import { MapService } from './../../shared/services/map.service';
import { ICarwash } from './../../shared/interfaces';
import { options } from './map.config';
import { IArea, IPoint } from './../../shared/interfaces';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  private map: google.maps.Map;
  private area = new Subject<IArea>();
  private markers: any[] = new Array();
  private carwashes: ICarwash[];

  constructor(private carwashService: CarwashService,
    private mapService: MapService,
    private router: Router) { }

  ngOnInit(): void {
    this.mapService.location.subscribe(
      data => this.updateMapCenter(data)
    );

    this.map = new google.maps.Map(document.getElementById('map'), options);
    this.setMapHeight();
    google.maps.event.addListener(this.map, 'bounds_changed', () => {
      this.areaUpdated();
    });
    this.area.debounceTime(500).distinctUntilChanged().subscribe(area => this.updateMarkers(area));
  }

  updateMarkers(area: IArea): void {
    this.carwashService.getCarwashesByArea(area).subscribe((data: ICarwash[]) => {
      let carwashes = data;
      this.displayCarwashesOnMap(carwashes);
    });
  }

  onResize(event): void {
    console.log("onResize(event)");
    this.setMapHeight();
  }

  updateMapCenter(latLng: google.maps.LatLng): void {
    console.log("updateMapCenter(latLng: google.maps.LatLng)");
    this.map.setCenter(latLng);
    google.maps.event.trigger(this.map, "resize");
  }

  areaUpdated(): void {
    this.area.next(this.getMapBounds());
  }

  private displayCarwashesOnMap(carwashes: ICarwash[]): void {
    console.log("displayCarwashesOnMap(carwashes: ICarwash[])");
    this.clearMarkers();
    if (carwashes) {
      for (let carwash of carwashes) {
        let marker = new google.maps.Marker({
          position: { lat: carwash.address.lat, lng: carwash.address.lng },
          title: carwash.name, icon: "/assets/icons/automatic.gif",
          map: this.map
        });
        let id: number = carwash.id;
        marker.setValues({ id: id });
        this.markers.push(marker);
        marker.addListener('click', (event) => {
          this.onMarkerClick(id);
        });
      }
      google.maps.event.trigger(this.map, "resize");
    }
  }

  private onMarkerClick(id: number): void {
    this.router.navigate(['/carwash', id]);
  }

  private getMapBounds(): IArea {
    let bounds = this.map.getBounds();
    let ne = bounds.getNorthEast();
    let sw = bounds.getSouthWest();
    let area: IArea = {
      ne: {
        lat: ne.lat(),
        lng: ne.lng()
      },
      sw: {
        lat: sw.lat(),
        lng: sw.lng()
      }
    }
    return area;
  }

  private setMapHeight(): void {
    (<HTMLElement>this.map.getDiv()).style.height = window.innerHeight - 100 + "px";
    google.maps.event.trigger(this.map, "resize");
  }

  private clearMarkers(): void {
    for (let marker of this.markers) {
      marker.setMap(null);
    }
  }
}