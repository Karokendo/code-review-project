import { Component, OnInit } from '@angular/core';
import { GoogleplaceDirective } from 'angular2-google-map-auto-complete/directives/googleplace.directive';
import { MapService } from './../../shared/services/map.service';

@Component({
    selector: 'map-search-cmp',
    templateUrl: './map-search.component.html',
    styleUrls: ['./map-search.component.css']
})
export class MapSearchComponent implements OnInit {

    public address: Object;

    constructor(private mapService: MapService) { }

    ngOnInit(): void {}

    getAddress(place: Object): void {
        this.address = place['formatted_address'];
        var location = place['geometry']['location'];
        var lat = location.lat();
        var lng = location.lng();
        console.log("Address Object", place);
        this.mapService.updateLocation(location);
    }


}
