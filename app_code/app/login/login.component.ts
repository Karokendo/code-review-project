import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthenticationService } from './../shared/services/authentication.service';
declare var $: any

@Component({
    selector: 'login-cmp',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {

    private loading: boolean = false;
    private model: any = {};

    constructor(private authenticationService : AuthenticationService) {}

    private login(): void {
        this.loading = true;
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(
            (data) =>  {
                this.loading = false;
                $('#loginModal').modal('hide');
                this.authenticationService.loggedIn.in = true;
            },
            error => {
                //this.alertService.error(error);
                this.loading = false;
            });
    }

}
